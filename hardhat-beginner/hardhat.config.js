require("@nomiclabs/hardhat-waffle");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */

// Go to https://www.alchemyapi.io, sign up, create
// a new App in its dashboard, and replace "KEY" with its key
// const ALCHEMY_API_KEY = "GfoNx3ioP3fnrt1XbmCu83LnJdMrRkrY"; // Robsten - The App
const ALCHEMY_API_KEY = "G6f1ye9ExOKPmsg0Rs-KhvPrFyljKUeU"; // Rinkeby - Demo App

// Replace this private key with your Ropsten account private key
// To export your private key from Metamask, open Metamask and
// go to Account Details > Export Private Key
// Be aware of NEVER putting real Ether into testing accounts
const MM_PRIVATE_KEY = process.env.MMK;

module.exports = {
    solidity: "0.7.3",
    networks: {
        ropsten: {
            url: `https://eth-ropsten.alchemyapi.io/v2/GfoNx3ioP3fnrt1XbmCu83LnJdMrRkrY`,
            accounts: [`${MM_PRIVATE_KEY}`],
        },
        rinkeby: {
            url: `https://eth-rinkeby.alchemyapi.io/v2/G6f1ye9ExOKPmsg0Rs-KhvPrFyljKUeU`,
            accounts: [`${MM_PRIVATE_KEY}`],
        },
    },
};
